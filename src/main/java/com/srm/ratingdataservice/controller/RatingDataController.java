package com.srm.ratingdataservice.controller;

import com.srm.ratingdataservice.model.Ratings;
import com.srm.ratingdataservice.model.UserRating;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/rating")
public class RatingDataController {

    @GetMapping("getRatings/{userId}")
    public ResponseEntity<?> getUserRatings(@PathVariable("userId") String userId){
        UserRating userRating =new UserRating();
        userRating.setRatings(Arrays.asList(new Ratings(213, "shole"),
                new Ratings(234, "Don")));

        return ResponseEntity.ok().body(userRating);
    }


}
