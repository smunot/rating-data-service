package com.srm.ratingdataservice.model;

public class Ratings {

    int rateing;
    String name;

    public Ratings(int rateing, String name) {
        this.rateing = rateing;
        this.name = name;
    }

    public int getRateing() {
        return rateing;
    }

    public void setRateing(int rateing) {
        this.rateing = rateing;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
