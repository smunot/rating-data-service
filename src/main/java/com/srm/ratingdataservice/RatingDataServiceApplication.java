package com.srm.ratingdataservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.core.env.Environment;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.net.InetAddress;
import java.net.UnknownHostException;
@EnableSwagger2
@SpringBootApplication
public class RatingDataServiceApplication extends SpringBootServletInitializer {

	public static final  String SERVER_PORT = "server.port";

	private static final Logger LOGGER = LoggerFactory.getLogger(RatingDataServiceApplication.class);

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(RatingDataServiceApplication.class);
	}
	public static void main(String[] args) throws UnknownHostException {

		SpringApplication app = new SpringApplication(RatingDataServiceApplication.class);
		Environment env = app.run(args).getEnvironment();
		LOGGER.info("\n------------------------------------\n\t"
						+"Aplication '{}' is running on! Access URLs:\n\t" + "Local:\t\thttp://localhost:{}\n\t"
						+"External: \thttp://{}:{}\n---------------------------------",
				env.getProperty("spring.application.name"),env.getProperty(SERVER_PORT),
				InetAddress.getLocalHost().getHostAddress(),env.getProperty(SERVER_PORT));
	}

}
